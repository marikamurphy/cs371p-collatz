// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair
#include <vector>   //for lazy cache

#include "Collatz.hpp"

using namespace std;

// -----------
// cycle_cache
// -----------

// used for caching the collatz cycle for numbers
// once it's computed
// note: it is a lazy cache
const int CACHE_SIZE = 500000;
int cycle_cache[CACHE_SIZE] = {0};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    // find the maximum cycle length
    // need handle input for either int in the pair to be greater

    int first, second, res;
    tie(first, second) = p;
    if( first < second) {
        res = max_cycle_len(first, second);
    }
    else {
        res = max_cycle_len(second, first);
    }

    return make_tuple(first, second, res);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}

// -------------
// max_cycle_len
// -------------

int max_cycle_len(int beg, int end) {
    // beg, end & max are ints
    // calculates the max collatz cycle length for the given range
    // from beg to end inclusive

    assert (beg <= end);

    // If b < (e / 2) + 1
    // then max_cycle_length(b, e) = max_cycle_length(m, e)
    if (beg < (end / 2 + 1)) {
        beg = end / 2 + 1;
    }

    int max_cycle = 0;
    int temp_max = 0;
    for (int num = beg; num < end + 1; ++num) {
        if (num < CACHE_SIZE) {
            if (cycle_cache[num] == 0) {
                temp_max = find_cycles(num);
                cycle_cache[num] = temp_max;
            }
            else {
                temp_max = cycle_cache[num];
            }
        }
        else {
            temp_max = find_cycles(num);
        }
        if (temp_max > max_cycle) {
            max_cycle = temp_max;
        }

    }

    assert (max_cycle >= 1);
    return max_cycle;
}

// -----------
// find_cycles
// -----------

int find_cycles(int orig_num) {
    // num & cycle_len are ints
    // computes & returns the collatz cycle length for the given num
    // utilizes cycle_cache to speed up

    int cycle_len = 1;
    long num = (long) orig_num;
    while (num != 1) {
        if (num >= CACHE_SIZE || cycle_cache[num] == 0) {
            if (num % 2 == 0) {
                num /= 2;
                cycle_len += 1;
            }
            else {
                // if x is odd, 3x+1 will always be even
                num = num + (num >> 1) + 1;
                cycle_len += 2;
            }
        }
        else {
            return (cycle_cache[num] + cycle_len - 1);
        }
    }

    assert (cycle_len >= 1);
    return cycle_len;
}
