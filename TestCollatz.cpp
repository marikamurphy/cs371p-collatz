// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

// -----
// max_cycle_len
// -----

TEST(CollatzFixture, max_cycle_len0) {
    ASSERT_EQ(max_cycle_len(1, 10), 20);
}

TEST(CollatzFixture, max_cycle_len1) {
    ASSERT_EQ(max_cycle_len(30, 90), 116);
}

TEST(CollatzFixture, max_cycle_len2) {
    ASSERT_EQ(max_cycle_len(1112, 1115), 138);
}

TEST(CollatzFixture, max_cycle_len3) {
    ASSERT_EQ(max_cycle_len(500, 1000), 179);
}



// -----
// find_cycles
// -----

TEST(CollatzFixture, cycle0) {
    ASSERT_EQ(find_cycles(1), 1);
}

TEST(CollatzFixture, cycle1) {
    ASSERT_EQ(find_cycles(2), 2);
}

TEST(CollatzFixture, cycle2) {
    ASSERT_EQ(find_cycles(3), 8);
}

TEST(CollatzFixture, cycle3) {
    ASSERT_EQ(find_cycles(4), 3);
}

TEST(CollatzFixture, cycle4) {
    ASSERT_EQ(find_cycles(7), 17);
}

TEST(CollatzFixture, cycle5) {
    ASSERT_EQ(find_cycles(150), 16);
}

TEST(CollatzFixture, cycle6) {
    ASSERT_EQ(find_cycles(123), 47);
}

TEST(CollatzFixture, cycle7) {
    ASSERT_EQ(find_cycles(1000), 112);
}